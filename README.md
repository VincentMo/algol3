taille 8  
hauteur 4  
parcours largeur 12 8 80 30 100 40 35 50  
  
préfixe  
12 8 80 30 40 35 50 100  
  
infixe  
8 12 30 35 40 50 80 100  
  
postfixe  
8 35 50 40 30 100 80 12  
  
ABR   

# Algo L3

![Screenshot Ex1](https://gitlab.com/VincentMo/algol3/-/raw/master/J2/patate.png)


# Exercice 1

![Screenshot Ex1](https://gitlab.com/VincentMo/algol3/-/raw/master/J1/ex1plot.PNG)

# Exercice 2

![Screenshot Ex1](https://gitlab.com/VincentMo/algol3/-/raw/master/J1/ex2.PNG)



