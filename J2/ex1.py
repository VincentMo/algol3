def searchItem( tab, item ):
    temp = 0
    while temp < len(tab):
        if tab[temp] == item:
            found = True
            return temp
        temp += 1
    return "Not found"

def searchItemSorted( tab, item ):
    temp = 0
    while temp < len(tab) and tab[temp] <= item:
        if tab[temp] == item:
            found = True
            return temp
        temp += 1
    return "Not found"

def searchDic(tab, item):
    return False

def termeDeLaSuite(firstItem, raison,n):
    temp = 0
    while temp < n:
        if temp == 0:
            Utemp = firstItem
        else:
            Utemp = Utemp + raison
        temp += 1
    return Utemp

def termeDeLaSuiteRec(firstItem, raison,n):
    if n <= 1 :
        return firstItem
    else
        return termeDeLaSuiteRec(firstItem, raison, n-1) + raison

if __name__ == "__main__":
    print("hello world")

