import numpy as np
from matplotlib import pyplot as plt

# Data
# Ensemble de données
x = range(1,11)

# f(n)
ylogn = np.log(x)
yn = x
ynlogn = x * np.log(x)
ynp2 = [temp ** 2 for temp in x]
y2pn = [2 ** temp for temp in x]


# Création des courbes
plt.plot(x,ylogn,"-g",label="f(n)=log(n)")
plt.plot(x,yn,"-y",label="f(n)=n")
plt.plot(x,ynlogn,"-r",label="f(n)=n log(n)")
plt.plot(x,ynp2,"-b",label="f(n)=n^2")
plt.plot(x,y2pn,"-",label="f(n)=2^n")

# Création du graphe
plt.legend()
plt.show()