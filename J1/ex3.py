def permutedQuadra(tab):
    temp1 = 0
    permuted = True
    while temp1 < len(tab) and permuted == True:
        temp2 = 0
        while temp2 < len(tab) and permuted == True:
            if temp1 != temp2 and tab[temp1] == tab[temp2] and tab[temp1] > 0:
                permuted = False
            temp2 += 1
        temp1 += 1
    return permuted


def permutedLineaire(tableau):
    tabResult = []
    ensembleDonnee =True
    # Initialisation tab
    for i in range(len(tableau) + 1):
        tabResult.append(0)

    # Vérification valeur entre 1 et n
    for i in tableau :
        if i < 1 or i > len(tableau) :
            ensembleDonnee = False

    # Comptage des occurence puis verification si aucune occurence supérieur a 1
    if ensembleDonnee :
        for i in tableau:
            tabResult[i] += 1
            if tabResult[i] > 1 :
                ensembleDonnee = False

    return ensembleDonnee


if __name__ == "__main__":
    tab = [1, 2, 3, 4, 5, 6]
    tab2 = [1, 3, 4, 5, 5]
    tab3 = [-1, 3, 4, 5, 5]

    print(permutedQuadra(tab))  # True
    print(permutedQuadra(tab2))  # False
    print(permutedQuadra(tab3))  # False
    print(permutedLineaire(tab))  # True
    print(permutedLineaire(tab2))  # False
    print(permutedLineaire(tab3))  # False

